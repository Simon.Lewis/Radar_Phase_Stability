V0.00000000000001
This code is completely open source and is available for altering how you see fit.
If you feel like giving me a credit, that would be appreciated =).


Welcome! This is my first git project, so be gentle coding gods.

The primary purpose of this project is to measure the phase of a static range bin
from a radar range/time profile. The reason for using range/time is that, ideally a static
target will have a 0Hz Doppler signature, but unfortunately this means it is buried in static
Doppler clutter and phase noise. I've contemplated using essentially a reversed MTI          (Moving Target Indicator) filter (STI?), which would essentially remove any range bins with targets that change w.r.t time. Maybe that becomes the next step, but for now, I'm developing a means of automatically registering targets based on their autocorrelation with a known reference function (see pulse compression), and then thresholding the data with a decaying exponential function based on the rate of power loss per metre (radars 'lose' power by a Range^-4 function). 
The threshold is a means of data reduction for rapid processing between pulses. 

I achieved some of this in Matlab, but the change to Python is a means of giving platform 
independent and non-propietry code to the public (go-go free information).

Things to do:

	Read .dat file into an array of ADC samples
	Match filter with a reference waveform 
	Build Range time profile
	Manually choose static range bins
	Read phase from static range and plot
	Continuous read and save to file
	
	Automatically find static targets
	Decision on static bin to track (threshold stability vs time?)
